<?php

declare(strict_types=1);

namespace Talentry\MonitoringBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    private const ROOT_NODE = 'monitoring';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);

        $treeBuilder
            ->getRootNode()
            ->children()
                ->booleanNode('enabled')->defaultValue(true)->end()
                ->scalarNode('statsdProtocol')->defaultValue('standard')->end()
                ->scalarNode('statsdHost')->defaultValue('localhost')->end()
                ->integerNode('statsdPort')->defaultValue(8125)->end()
                ->scalarNode('queue')->defaultValue(null)->end()
                ->scalarNode('environment')
                    ->defaultValue(null)
                    ->setDeprecated('', '', 'Deprecated; use namespace instead')
                ->end()
                ->scalarNode('namespace')->defaultValue(null)->end()
                ->scalarNode('redisHost')->defaultValue('localhost')->end()
                ->integerNode('redisPort')->defaultValue(6379)->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
