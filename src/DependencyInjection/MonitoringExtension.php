<?php

declare(strict_types=1);

namespace Talentry\MonitoringBundle\DependencyInjection;

use Predis\Client;
use Predis\ClientInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Talentry\Monitoring\Infrastructure\Monitor\MonitorFactory;
use Talentry\Monitoring\Infrastructure\Queue\InMemoryQueue;
use Talentry\Monitoring\Infrastructure\Queue\RedisQueue;
use Talentry\Monitoring\Infrastructure\Metric\MetricStoreFactory;

class MonitoringExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $config = $this->processConfiguration(new Configuration(), $configs);

        $metricStoreFactory = $container->findDefinition(MetricStoreFactory::class);

        $metricStoreFactory->setArgument('$statsdHost', $config['statsdHost']);
        $metricStoreFactory->setArgument('$statsdPort', $config['statsdPort']);
        $metricStoreFactory->setArgument('$statsdProtocol', $config['statsdProtocol']);

        $monitorFactory = $container->findDefinition(MonitorFactory::class);
        $monitorFactory->setArgument('$enabled', $config['enabled']);
        $namespace = $config['namespace'] ?? $config['environment'];
        $monitorFactory->setArgument('$namespace', $namespace);

        switch ($config['queue']) {
            case 'redis':
                $redisClient = $this->createRedisClientDefinition($config['redisHost'], $config['redisPort']);
                $queue = new Definition(RedisQueue::class, [$redisClient]);
                break;
            case 'memory':
                $queue = new Definition(InMemoryQueue::class);
                break;
            default:
                $queue = null;
        }

        $monitorFactory->setArgument('$queue', $queue);
    }

    private function createRedisClientDefinition(string $redisHost, int $redisPort): Definition
    {
        $redisUri = sprintf('tcp://%s:%s', $redisHost, $redisPort);
        $client = new Definition(ClientInterface::class, [$redisUri]);
        $client->setClass(Client::class);

        return $client;
    }
}
