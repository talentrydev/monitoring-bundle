<?php

declare(strict_types=1);

namespace Talentry\MonitoringBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MonitoringBundle extends Bundle
{
}
