<?php

declare(strict_types=1);

namespace Talentry\MonitoringBundle\Tests;

use Exception;
use Predis\Client;
use Predis\Response\Status;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Talentry\Monitoring\Infrastructure\Queue\QueuedMetricsProcessor;
use Talentry\Monitoring\Tests\Mock\MetricsWithTags\IncrementMetricWithTags;

class ConfigurationTest extends KernelTestCase
{
    private $socket;
    private int $port;
    private Metric $metric;
    private Metric $metricWithTags;
    private int $currentTimestamp;
    private string $awsEmfLogGroupName = '/aws/emf/metrics/';

    protected function setUp(): void
    {
        parent::setUp();

        $host = '127.0.0.1';
        $this->port = 12_345;
        $this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        $this->metric = Metrics::incrementMetric('test');
        $this->metricWithTags = new IncrementMetricWithTags('test', ['foo' => 'bar', 'bar' => 'baz']);
        socket_bind($this->socket, $host, $this->port);
        socket_set_option($this->socket, SOL_SOCKET, SO_RCVTIMEO, ['sec' => 1, 'usec' => 0]);
        $this->currentTimestamp = 1644505630691; //hardcoded timestamp in MockTimeProvider
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        socket_close($this->socket);
        putenv('ENABLED');
    }

    public function testWithStandardStatsdProtocol(): void
    {
        $this->givenThatBundleIsConfiguredNotToUseQueue();
        $this->givenThatStatsdProtocolIsStandard();
        $this->whenMetricWithTagsIsPushed();
        $this->expectMetricWithTagsToBeSentInStandardFormat();
    }

    public function testWithAwsEmfStatsdProtocol(): void
    {
        $this->givenThatBundleIsConfiguredNotToUseQueue();
        $this->givenThatStatsdProtocolIsAwsEmf();
        $this->whenMetricIsPushed();
        $this->expectMetricToBeSentInAwsEmfFormat();
    }

    public function testWithAwsEmfStatsdProtocolAndNamespace(): void
    {
        $this->givenThatBundleIsConfiguredNotToUseQueue();
        $this->givenThatStatsdProtocolIsAwsEmfAndNamespaceIsConfigured();
        $this->whenMetricIsPushed();
        $this->expectMetricWithNamespaceToBeSentInAwsEmfFormat();
    }

    public function testWithEnvironment(): void
    {
        $this->givenThatEnvironmentIsConfigured();
        $this->whenMetricIsPushed();
        $this->expectMetricToContainNamespaceAsPrefix();
    }

    public function testWithNamespace(): void
    {
        $this->givenThatNamespaceIsConfigured();
        $this->whenMetricIsPushed();
        $this->expectMetricToContainNamespaceAsPrefix();
    }

    public function testWithInMemoryQueue(): void
    {
        $this->givenThatBundleIsConfiguredToUseInMemoryQueue();
        $this->whenMetricIsPushed();
        $this->expectNoMetricsToBeSent();

        $this->whenQueueIsProcessed();
        $this->expectMetricToBeSent();
    }

    public function testWithRedisQueue(): void
    {
        $this->givenThatBundleIsConfiguredToUseRedisQueue();
        $this->givenThatRedisIsAvailable();
        $this->whenMetricIsPushed();
        $this->expectNoMetricsToBeSent();

        $this->whenQueueIsProcessed();
        $this->expectMetricToBeSent();
    }

    public function testDisablingTheBundle(): void
    {
        $this->givenThatBundleIsConfiguredToBeDisabled();
        $this->whenMetricIsPushed();
        $this->expectNoMetricsToBeSent();
    }

    public function testDisablingTheBundleViaEnvVar(): void
    {
        $this->givenThatBundleIsConfiguredToBeDisabledViaEnvVar();
        $this->whenMetricIsPushed();
        $this->expectNoMetricsToBeSent();
    }

    public function testCommandsAreRegistered(): void
    {
        $this->givenThatBundleIsConfiguredNotToUseQueue();
        $this->expectCommandsToBeExecutable();
    }

    private function givenThatBundleIsConfiguredNotToUseQueue(): void
    {
        self::bootKernel(['environment' => 'no_queue']);
    }

    private function givenThatBundleIsConfiguredToUseInMemoryQueue(): void
    {
        self::bootKernel(['environment' => 'memory']);
    }

    private function givenThatBundleIsConfiguredToUseRedisQueue(): void
    {
        self::bootKernel(['environment' => 'redis']);
    }

    private function givenThatStatsdProtocolIsStandard(): void
    {
        self::bootKernel(['environment' => 'standard']);
    }

    private function givenThatStatsdProtocolIsAwsEmf(): void
    {
        self::bootKernel(['environment' => 'aws_emf']);
    }

    private function givenThatStatsdProtocolIsAwsEmfAndNamespaceIsConfigured(): void
    {
        self::bootKernel(['environment' => 'aws_emf_with_namespace']);
    }

    private function givenThatEnvironmentIsConfigured(): void
    {
        self::bootKernel(['environment' => 'environment']);
    }

    private function givenThatNamespaceIsConfigured(): void
    {
        self::bootKernel(['environment' => 'namespace']);
    }

    private function givenThatBundleIsConfiguredToBeDisabled(): void
    {
        self::bootKernel(['environment' => 'disabled']);
    }

    private function givenThatBundleIsConfiguredToBeDisabledViaEnvVar(): void
    {
        putenv('ENABLED=false');
        self::bootKernel(['environment' => 'disabled_via_env_var']);
    }

    private function whenMetricIsPushed(): void
    {
        $monitor = self::getContainer()->get(Monitor::class);
        $monitor->push($this->metric);
    }

    private function whenMetricWithTagsIsPushed(): void
    {
        $monitor = self::getContainer()->get(Monitor::class);
        $monitor->push($this->metricWithTags);
    }

    private function whenQueueIsProcessed(): void
    {
        self::getContainer()->get(QueuedMetricsProcessor::class)->run();
    }

    private function expectMetricWithTagsToBeSentInStandardFormat(): void
    {
        self::assertSame('test:1|c|#foo:bar,bar:baz', $this->readSocket());
    }

    private function expectMetricToBeSentInAwsEmfFormat(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->awsEmfLogGroupName . 'Production',
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => 'Production',
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => 'test']],
                    ]
                ],
            ],
            'test' => 1
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    private function expectMetricWithNamespaceToBeSentInAwsEmfFormat(): void
    {
        $expectedJson = [
            '_aws' => [
                'Timestamp' => $this->currentTimestamp,
                'LogGroupName' => $this->awsEmfLogGroupName . 'dev',
                'CloudWatchMetrics' => [
                    [
                        'Namespace' => 'dev',
                        'Dimensions' => [[]],
                        'Metrics' => [['Name' => 'test']],
                    ]
                ],
            ],
            'test' => 1
        ];

        self::assertSame(json_encode($expectedJson), $this->readSocket());
    }

    private function expectMetricToBeSent(): void
    {
        self::assertSame('test:1|c', $this->readSocket());
    }

    private function expectMetricToContainNamespaceAsPrefix(): void
    {
        self::assertSame('dev.test:1|c', $this->readSocket());
    }

    private function expectNoMetricsToBeSent(): void
    {
        self::assertEmpty($this->readSocket());
    }

    private function expectCommandsToBeExecutable(): void
    {
        $application = new Application(self::$kernel);
        $application->setAutoExit(false);

        $commands = [
            'ty:monitoring:push-queued-metrics',
            'ty:monitoring:push-test-metric',
        ];

        foreach ($commands as $command) {
            $input = new ArrayInput(['command' => $command]);
            $exit = $application->run($input, new NullOutput());
            self::assertSame(0, $exit, $command);
        }
    }

    private function givenThatRedisIsAvailable(): void
    {
        $redisHost = getenv('REDIS_HOST');
        $redisPort = getenv('REDIS_PORT');
        if (!$redisHost || !$redisPort) {
            self::markTestSkipped('Redis host / port not configured');
        }

        $client = new Client(sprintf('tcp://%s:%s', $redisHost, $redisPort));
        try {
            $response = $client->ping();
            if (!$response instanceof Status || $response->getPayload() !== 'PONG') {
                throw new Exception();
            }
        } catch (Exception $e) {
            self::markTestSkipped('Redis unavailable');
        }
    }

    private function readSocket(): string
    {
        $name = null;
        $buffer = '';

        socket_recvfrom($this->socket, $buffer, 1024, 0, $name, $this->port);

        return $buffer;
    }
}
