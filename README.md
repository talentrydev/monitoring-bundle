# Monitoring bundle

This is a Symfony bundle used for integrating talentrydev/monitoring library into a Symfony project.

## Installing

* Run:

```
composer require talentrydev/monitoring-bundle
```

* Add the MonitoringBundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\MonitoringBundle\MonitoringBundle();
];
```

# Configuring

The bundle exposes the following configuration options:

| Option         | Type    | Default value | Description                                           |
|----------------|---------|---------------|-------------------------------------------------------|
| enabled        | boolean | true          | Set to false to disable monitoring bundle             |
| namespace      | string  | Production    | Namespace for publishing metrics                      |
| statsdProtocol | string  | standard      | StatsD protocol (see below for more details)          |
| statsdHost     | string  | localhost     | StatsD server host                                    |
| statsdPort     | integer | 8125          | StatsD server port                                    |
| queue          | string  | null          | Queueing strategy to use (see below for more details) |
| redisHost      | string  | localhost     | Host of the redis instance                            |
| redisPort      | integer | 6379          | Port of the redis instance                            |

- `statsdProtocol` may be either `datadog` (for datadog extension of StatsD protocol) or `standard` (for standard StatsD protocol)
- `queue` may be either `redis`, `memory` or `null` (default). By setting this value to `redis` or `memory`
    the metrics will be queued using the selected strategy and will only be published once the
    `ty:monitoring:push-queued-metrics` command has been executed.

## Example configuration

- To configure the bundle to use the standard StatsD protocol, with StatsD agent listening on `10.10.10.10`, port `8125`:

```
monitoring:
  statsdHost: 10.10.10.10
```

- To configure the bundle to use the redis queue, add the following to `config.yml`:

```
monitoring:
  queue: redis
```
